﻿using System;

namespace Sales.App.Common.Models
{
    public class SalesInfo
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double DollarAmount { get; set; }
    }
}
