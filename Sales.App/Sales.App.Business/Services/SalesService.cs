﻿using Sales.App.Business.Interfaces;
using Sales.App.Common.Models;
using Sales.App.Data.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sales.App.Business.Services
{
    public class SalesService : ISalesService<SalesInfo>
    {

        private readonly ISalesInfoRepository<SalesInfo> _salesInfoRepository;

        public SalesService(ISalesInfoRepository<SalesInfo> salesInfoRepository)
        {
            _salesInfoRepository = salesInfoRepository;
        }

        public async Task<SalesInfo> Create(SalesInfo salesInfo)
        {
            try
            {
                return await _salesInfoRepository.Create(salesInfo);
            }
            catch
            {
                return await Task.FromResult<SalesInfo>(null);
            }

        }

        public async Task Remove(int id)
        {
            await _salesInfoRepository.Remove(id);
        }

        public Task<List<SalesInfo>> GetAll()
        {
            return _salesInfoRepository.GetAll();
        }

        public Task<SalesInfo> Get(int id)
        {
            return _salesInfoRepository.Get(id);
        }
    }
}
