﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sales.App.Business.Interfaces
{
    public interface IService<T> where T : class
    {
        Task<T> Get(int id);
        Task<T> Create(T item);
        Task<List<T>> GetAll();
        Task Remove(int id);
    }
}
