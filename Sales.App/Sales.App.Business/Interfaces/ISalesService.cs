﻿namespace Sales.App.Business.Interfaces
{
    public interface ISalesService<T> : IService<T> where T : class
    {
    }
}
