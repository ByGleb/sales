﻿using Microsoft.Extensions.DependencyInjection;
using Sales.App.Business.Interfaces;
using Sales.App.Business.Services;
using Sales.App.Common.Models;

namespace Sales.App.Business.DI
{
    public static class BusinessDependencyRegistrator
    {
        public static void RegisterBusinessServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<ISalesService<SalesInfo>, SalesService>();
        }
    }
}
