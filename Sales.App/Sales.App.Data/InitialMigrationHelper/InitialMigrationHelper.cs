﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Sales.App.Data.DataAccess;
using System;
using System.Threading.Tasks;

namespace Sales.App.Data.InitialMigrationHelper
{
    public static class InitialMigrationHelper
    {
        public static async Task EnsureDatabasesMigrated(IServiceProvider services)
        {
            using (var scope = services.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                using (var context = scope.ServiceProvider.GetRequiredService<SalesGraphContext>())
                {
                    await context.Database.MigrateAsync();
                }
            }
        }
    }
}
