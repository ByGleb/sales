﻿using Microsoft.EntityFrameworkCore;
using Sales.App.Common.Models;

namespace Sales.App.Data.DataAccess
{
    public class SalesGraphContext : DbContext
    {
        public DbSet<SalesInfo> SalesInfos { get; set; }

        public SalesGraphContext(DbContextOptions<SalesGraphContext> options)
            : base(options)
        {
        }
    }
}
