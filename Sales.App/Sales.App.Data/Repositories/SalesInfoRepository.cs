﻿using Microsoft.EntityFrameworkCore;
using Sales.App.Common.Models;
using Sales.App.Data.DataAccess;
using Sales.App.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sales.App.Data.Repositories
{
    public class SalesInfoRepository : ISalesInfoRepository<SalesInfo>
    {
        private readonly SalesGraphContext _context;

        public SalesInfoRepository(SalesGraphContext context)
        {
            _context = context;
        }

        public async Task<List<SalesInfo>> GetAll()
        {
            return await _context.SalesInfos.ToListAsync();
        }

        public async Task<SalesInfo> Get(int id)
        {
            return await _context.SalesInfos.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<SalesInfo> Create(SalesInfo item)
        {
            var entity = await _context.SalesInfos.FirstOrDefaultAsync(x => x.Id == item.Id);

            if (entity == null)
            {
                item.Date = DateTime.Now;
                await _context.AddAsync(item);
                await _context.SaveChangesAsync();
                return item;
            }

            return null;
        }

        public async Task Remove(int id)
        {
            var entity = await _context.SalesInfos.FirstOrDefaultAsync(x => x.Id == id);

            if (entity != null)
            {
                _context.SalesInfos.Remove(entity);
                await _context.SaveChangesAsync();
            }
        }
    }
}
