﻿namespace Sales.App.Data.Interfaces
{
    public interface ISalesInfoRepository<T> : IRepository<T> where T : class
    {
    }
}
