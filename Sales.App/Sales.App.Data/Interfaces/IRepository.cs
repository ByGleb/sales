﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sales.App.Data.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<T> Get(int id);
        Task<T> Create(T item);
        Task<List<T>> GetAll();
        Task Remove(int id);
    }
}