﻿using Microsoft.Extensions.DependencyInjection;
using Sales.App.Common.Models;
using Sales.App.Data.Interfaces;
using Sales.App.Data.Repositories;

namespace Sales.App.Data.DI
{
    public static class DataDependencyRegistrator
    {
        public static void RegisterDataRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<ISalesInfoRepository<SalesInfo>, SalesInfoRepository>();
        }
    }
}
